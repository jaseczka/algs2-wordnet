import java.util.Arrays;
import java.util.Collection;

/**
 * Created by mtepe_000 on 2014-11-14.
 */
public class SAP {
    private Digraph digraph;
    private java.util.Map<Integer, Integer> bfsV;
    private java.util.Map<Integer, Integer> bfsW;

    private static class PathData {
        private int pathLength;
        private int commonAncestor;

        public PathData() {
            this.pathLength = -1;
            this.commonAncestor = -1;
        }

        public PathData(int length, int ancestor) {
            this.pathLength = length;
            this.commonAncestor = ancestor;
        }

        public int getPathLength() {
            return this.pathLength;
        }

        public int getCommonAncestor() {
            return this.commonAncestor;
        }

        public boolean isBetter(PathData other) {
            return isBetter(other.pathLength);
        }

        public boolean isBetter(int length) {
            return (this.pathLength == -1 || length < this.pathLength);
        }
    }

    // constructor takes a digraph (not necessarily a DAG)
    public SAP(Digraph G) {
        //copy
        this.digraph = new Digraph(G);
        this.bfsV = new java.util.HashMap<Integer, Integer>();
        this.bfsW = new java.util.HashMap<Integer, Integer>();
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        return findSAP(v, w).getPathLength();
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
        return findSAP(v, w).getCommonAncestor();
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        return findSAP(v, w).getPathLength();
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        return findSAP(v, w).getCommonAncestor();
    }

    private PathData findSAP(int i, int j) {
        java.util.List<Integer> v = new java.util.LinkedList<Integer>();
        java.util.List<Integer> w = new java.util.LinkedList<Integer>();
        v.add(i);
        w.add(j);
        return findSAP(v, w);
    }

    private void fillBfsMap(java.util.Map<Integer, Integer> bfsMap, Iterable<Integer> v) {
        java.util.Queue<Integer> q = new java.util.LinkedList<Integer>();
        for (int i : v) {
            if (i < 0 || i >= digraph.V()) {
                throw new IndexOutOfBoundsException();
            }
            q.add(i);
            bfsMap.put(i, 0);
        }
        while (!q.isEmpty()) {
            int w = q.remove();
            int distance = bfsMap.get(w);
            for (int adj : digraph.adj(w)) {
                if (!bfsMap.containsKey(adj)) {
                    q.add(adj);
                    bfsMap.put(adj, distance + 1);
                }
            }
        }
    }

    private PathData findSAP(Iterable<Integer> v, Iterable<Integer> w) {
        bfsV.clear();
        bfsW.clear();
        fillBfsMap(bfsV, v);
        fillBfsMap(bfsW, w);
        PathData bestPath = new PathData();
        for (int key : bfsV.keySet()) {
            if (bfsW.containsKey(key)) {
                PathData candidate = new PathData(bfsV.get(key) + bfsW.get(key), key);
                if (bestPath.isBetter(candidate)) {
                    bestPath = candidate;
                }
            }
        }

        return bestPath;
    }

    private static void smallDigraphTest() {
        StdOut.println("********************************");
        StdOut.println("smallDigraphTest");
        In in = new In("wordnet-testing\\wordnet\\digraph2.txt");
        Digraph d = new Digraph(in);
        SAP sap = new SAP(d);
        StdOut.println("Ancestor of 0 and 0 should be 0..." + sap.ancestor(0, 0));
        StdOut.println("Ancestor of 0 and 1 should be 0..." + sap.ancestor(0, 1));
        StdOut.println("Ancestor of 1 and 5 should be 0..." + sap.ancestor(1, 5));
        StdOut.println("SAP length between 0 and 0 should be 0..." + sap.length(0, 0));
        StdOut.println("SAP length between 0 and 1 should be 1..." + sap.length(0, 1));
        StdOut.println("SAP length between 1 and 5 should be 2..." + sap.length(1, 5));
        Integer[] v = {0, 1};
        Integer[] w = {3, 4};
        Integer[] x = {2};
        Integer[] y = {1};
        StdOut.println("Ancestor of {0, 1} and {3, 4} should be 0..." + sap.ancestor(Arrays.asList(v), Arrays.asList(w)));
        StdOut.println("Ancestor of {3, 4} and {2} should be 3..." + sap.ancestor(Arrays.asList(w), Arrays.asList(x)));
        StdOut.println("Ancestor of {1} and {2} should be 2..." + sap.ancestor(Arrays.asList(y), Arrays.asList(x)));
        StdOut.println("SAP distance of {0, 1} and {3, 4} should be 2..." + sap.length(Arrays.asList(v), Arrays.asList(w)));
        StdOut.println("SAP distance of {3, 4} and {2} should be 1..." + sap.length(Arrays.asList(w), Arrays.asList(x)));
        StdOut.println("SAP distance of {1} and {2} should be 1..." + sap.length(Arrays.asList(y), Arrays.asList(x)));

        StdOut.println("finished.");
    }

    // do unit testing of this class
    public static void main(String[] args) {
        smallDigraphTest();
    }
}

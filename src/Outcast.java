
/**
 * Created by mtepe_000 on 2014-11-14.
 */
public class Outcast {
    private WordNet wordnet;

    public Outcast(WordNet wordnet) {
        this.wordnet = wordnet;
    }

    public String outcast(String[] nouns) {
        String outcastCandidate = null;
        int outcastCandidateDistance = -1;

        for (String lhs : nouns) {
            int d = 0;
            for (String rhs : nouns) {
                d += wordnet.distance(lhs, rhs);
            }
            if (d > outcastCandidateDistance) {
                outcastCandidate = lhs;
                outcastCandidateDistance = d;
            }

        }

        return outcastCandidate;
    }

    private static void smallWordNetTest() {
        StdOut.println("********************************");
        StdOut.println("smallWordNetTest");
        WordNet wn = new WordNet("wordnet-testing\\wordnet\\synsets6.txt", "wordnet-testing\\wordnet\\hypernyms6TwoAncestors.txt");
        Outcast o = new Outcast(wn);
        java.util.ArrayList<String> nounsAL = new java.util.ArrayList<String>();
        for (String s : wn.nouns()) {
            nounsAL.add(s);
        }
        String[] nouns = nounsAL.toArray(new String[nounsAL.size()]);
        StdOut.println("All nouns:");
        StdOut.println(wn.nouns().toString());
        StdOut.println("All nouns outcast should be a or c..." + o.outcast(nouns));
        String[] b = {"b"};
        StdOut.println("[b] outcast should be b..." + o.outcast(b));
        String[] bfc = {"b", "f", "c"};
        StdOut.println("[b, f, c] outcast should be f..." + o.outcast(bfc));
        StdOut.println("finished.");
    }

    public static void main(String[] args) {
        smallWordNetTest();
    }
}

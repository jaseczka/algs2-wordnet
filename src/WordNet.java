/**
 * Created by mtepe_000 on 2014-11-14.
 */
public class WordNet {

    private Digraph wordnet;
    private java.util.List<Synset> synsets;
    private java.util.Map<String, java.util.Set<Integer>> nouns;
    private SAP sap;

    private static class Synset {
        private int id;
        private java.util.Set<String> synset;
        private String synsetString;
        private String definition;

        public Synset(int id, String synsetString, String definition) {
            this.id = id;
            this.synsetString = synsetString;
            this.synset = new java.util.HashSet<String>(java.util.Arrays.asList(synsetString.split(" ")));
            this.definition = definition;
        }

        public int getId() {
            return this.id;
        }

        public java.util.Set<String> getSynset() {
            return this.synset;
        }

        public String getSynsetString() {
            return this.synsetString;
        }

        public String getDefinition() {
            return this.definition;
        }

        public String toString() {
            return Integer.toString(this.id) + ", " + this.synsetString + ", def. " + this.definition;
        }
    }

    // constructor takes the name of the two input files
    public WordNet(String synsetsFilePath, String hypernymsFilePath) {
        this.synsets = new java.util.ArrayList<Synset>();
        this.nouns = new java.util.HashMap<String, java.util.Set<Integer>>();
        addSynsetsFromFile(synsetsFilePath);

        this.wordnet = new Digraph(this.synsets.size());
        addHypernymsFromFile(hypernymsFilePath);
        throwExceptionIfNotRootedDAG();

        this.sap = new SAP(wordnet);
        StdOut.println(summary());
    }

    private void throwExceptionIfNotRootedDAG() {
        throwExceptionIfNotRooted();
        throwExceptionIfNotDAG();
    }

    private void throwExceptionIfNotRooted() {
        boolean foundRootCandidate = false;
        for (int v = 0; v < wordnet.V(); ++v) {
            if (!wordnet.adj(v).iterator().hasNext()) {
                if (!foundRootCandidate) {
                    foundRootCandidate = true;
                } else {
                    throw new IllegalArgumentException();
                }
            }
        }
        if (!foundRootCandidate) {
            throw new IllegalArgumentException();
        }
    }

    private void throwExceptionIfNotDAG() {

    }

    private void addHypernymsFromFile(String filePath) {
        In hypernymsIn = new In(filePath);
        while (hypernymsIn.hasNextLine()) {
            addHypernymsFromLine(hypernymsIn.readLine());
        }
    }

    private void addHypernymsFromLine(String line) {
        String[] hypernyms = line.split(",");
        int synset = Integer.parseInt(hypernyms[0]);
        for (int i = 1; i < hypernyms.length; ++i) {
            int hypernym = Integer.parseInt(hypernyms[i]);
            wordnet.addEdge(synset, hypernym);
        }
    }

    private void addSynsetsFromFile(String filePath) {
        In synsetsIn = new In(filePath);
        while (synsetsIn.hasNextLine()) {
            Synset s = makeSynsetFromLine(synsetsIn.readLine());
            this.synsets.add(s);
            for (String noun : s.getSynset()) {
                java.util.Set<Integer> synsetSet = nouns.get(noun);
                if (synsetSet == null) {
                    synsetSet = new java.util.HashSet<Integer>();
                }
                synsetSet.add(s.getId());
                this.nouns.put(noun, synsetSet);
            }
        }
        synsetsIn.close();
    }

    private static Synset makeSynsetFromLine(String line) {
        String[] splittedLine = line.split(",");

        int id = Integer.parseInt(splittedLine[0]);
        String syn = splittedLine[1];
        String definition = splittedLine[2];

        Synset s = new Synset(id, syn, definition);

        return s;
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {
        return this.nouns.keySet();
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        if (word == null) {
            throw new NullPointerException();
        }
        return this.nouns.containsKey(word);
    }

    // distance between nounA and nounB
    public int distance(String nounA, String nounB) {
        if (!isNoun(nounA) || !isNoun(nounB)) {
            throw new IllegalArgumentException();
        }
        return sap.length(nouns.get(nounA), nouns.get(nounB));
    }

    // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
    // in a shortest ancestral path
    public String sap(String nounA, String nounB) {
        int ancestor = sap.ancestor(nouns.get(nounA), nouns.get(nounB));
        return this.synsets.get(ancestor).getSynsetString();
    }

    private String summary() {
        return "[WordNet: " + wordnet.V() + " synsets, " + nouns.size() + " nouns]";
    }

    // do unit testing of this class
    public static void main(String[] args) {
        smallWordNetTest();
        bigWordNetTest();
    }

    private static void smallWordNetTest() {
        StdOut.println("********************************");
        StdOut.println("smallWordNetTest");
        WordNet wn = new WordNet("wordnet-testing\\wordnet\\synsets6.txt", "wordnet-testing\\wordnet\\hypernyms6TwoAncestors.txt");
        StdOut.println("All nouns:");
        StdOut.println(wn.nouns().toString());
        StdOut.print("Distance b - f should be 2...");
        StdOut.println(wn.distance("b", "f"));
        StdOut.print("Distance b - c should be 1...");
        StdOut.println(wn.distance("b", "c"));
        StdOut.print("Distance b - a should be 1...");
        StdOut.println(wn.distance("b", "a"));
        StdOut.print("Distance e - c should be 2...");
        StdOut.println(wn.distance("e", "c"));
        StdOut.print("Distance b - e should be 3...");
        StdOut.println(wn.distance("b", "e"));
        StdOut.print("Distance d - d should be 0...");
        StdOut.println(wn.distance("d", "d"));
        StdOut.println("finished.");
    }

    private static void bigWordNetTest() {
        StdOut.println("********************************");
        StdOut.println("bigWordNetTest");
        WordNet wn = new WordNet("wordnet-testing\\wordnet\\synsets.txt", "wordnet-testing\\wordnet\\hypernyms.txt");
        StdOut.println("finished.");
        StdOut.println("Distance Black_Plague - black_marlin should be 33..." + wn.distance("Black_Plague", "black_marlin"));
    }
}

# Coursera Algorithms II Homework #1 #
## Directed graph search exercise ##

[Assignment details](http://coursera.cs.princeton.edu/algs4/assignments/wordnet.html)

Score: 103.12% (bonus for good code performance)